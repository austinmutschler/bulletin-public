<?php

session_start ();

// If user is logged in
if (isset ( $_SESSION ['user_id'] )) {
	header ( "Location: board.php" );
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Bulletin - Login</title>
<link href="assets/style/start.css" type="text/css" rel="stylesheet">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Roboto|Titillium+Web" rel="stylesheet">
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="assets/js/loginregisterhandler.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-100420011-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
	<div id="overlay">
		<div id="formcontainer">
			<p id="loginheader">Bulletin</p>
				<div id="loginformblock">
					<input class="input-login-email" type="email" name="email" placeholder="Email" autofocus required>
					<input class="input-login-password" type="password" name="password" placeholder="Password" required>
					<p>
						<input id="login-button" class="mainbuttons" type="submit" name="login" value="Login">
						<a class="elsetext"> or </a> <a class="orhyper" href="register.php">Register</a>
					</p>
				</div>
			<div id="error-message"></div>
		</div>
	</div>
</body>
</html>

