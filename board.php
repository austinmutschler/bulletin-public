<?php
session_start ();
date_default_timezone_set('America/Phoenix');
include 'assets/scripts/model.php';
$accountDatabaseAdapter = new accountDatabaseAdapter ();

$_POST ['form'] = '';

// If user is not logged in
if (! isset ( $_SESSION ['user_id'] )) {
	header ( "Location: login.php" );
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Bulletin - Board</title>
<link href="assets/style/main.css" type="text/css" rel="stylesheet">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400|Titillium+Web" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/userselectzip.js"></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-100420011-1', 'auto');
	//ga('send', 'pageview', {'page': location.pathname + location.search  + location.hash});
</script>
</head>
<body>
	<div id="top-nav-bar">
		<div id="head">
			<div class="logo">
				<h1 class="textlogo">Bulletin</h1>
			</div>
			<div class="headright">
				<div class="dropdown">
					<div id="usersname">Hello, <?php echo $_SESSION['user_first_name'];?></div>
					<div class="dropdown-content">
						<a href="assets/scripts/logout.php">Logout</a>
					</div>
				</div>
			</div>

			<div id="searcharea">
				<div class="mainsearch">

					<form id="searchForm" action="assets/scripts/controller.php" method="post">

						<input type="hidden" name="form" value="searchSeekingOffering" />

						<input class="searchinput" type="text" name="search" placeholder="Search">

						<select id="categories" class="categories" name="categories">
						
						<?php
							if (isset ( $_GET ['category'] ) && $_GET ['category'] === 'All Categories') {
								echo file_get_contents ( '/assets/categories.txt', true );
								
							} 
							else if (isset ( $_GET ['category'] )) {
								echo '<option selected="selected" value="' . $_GET ['category'] . '">' . $_GET ['category'] . '</option>';
								echo file_get_contents ( '/assets/categories.txt', true );
							} 

							else {
								echo file_get_contents ( './assets/categories.txt', true );
							}
						?>
						</select>
						<button id="searchbutton">Search</button>
					</form>
				</div>
					<div id="location">
						<?php
							if($_SESSION ['user_zip'] == "")
								echo "Zip Code: <span class='userzip'>Click Here to Enter Your Zip Code</span>";
							else
								echo "Zip Code: <span class='userzip'>" . $_SESSION ['user_zip'] . "</span>";
						?>					
					</div>
				</div>
			</div>

		<div class="tab">
			<button id="OfferingButton" class="sectionl" onclick="openPage('Offering', '')">Offering</button>
			<button id="SeekingButton" class="sectionl" onclick="openPage('Seeking', '')">Seeking</button>
			<button id="MyProfileButton" class="sectionl" onclick="openPage('MyProfile', '')">My Profile</button>
			<?php
				if ($accountDatabaseAdapter -> getUserType($_SESSION ['user_id']) == "admin") {
					echo '<button id="AdminPanelButton" class="sectionl" onclick="openPage(\'AdminPanel\', \'\')">Admin Panel</button>';
				}
			?>

			<div id="newpostbutton">
				<button class="mainbuttons">Create Post</button>
			</div>
		</div>
	</div>

	<div id="backgroundoverlay">
		<div id="OfferingSection" class="boardcontent">
			<?php
				if (isset ( $_GET ['category'] ) && ($_GET ['category'] !== 'All Categories')) {
					$arr = $accountDatabaseAdapter->searchOfferingPosts( $_GET ['category'] );
				} 
				else {
					$arr = $accountDatabaseAdapter->getAllOfferingPosts();
				}
				
				if (sizeof ( $arr ) == 0 && isset($_GET ['category'])) {
					$result = 'There are currently no offering posts for ' . $_GET ['category'];
					echo $result;
				} 
				else {
					$result = '';
					
					foreach ( array_reverse ( $arr ) as $item ) {
						
						$result = '<div class="offeringposttemplate">
										<div class="nameandoccupation">
											<p class="usersnamefield">' . $accountDatabaseAdapter -> getUsersName($item ['uid']) . '</p>
											<p class="userinputtedoccupation">' . $item ['occupation'] . '</p>
										</div>
										<div class="offeringuserratingbox">
											<p class="offeringrating">' . $accountDatabaseAdapter -> getUsersOfferingScore($item ['uid'])[0] . '</p>
											<p class="offeringnumofrating">(' . $accountDatabaseAdapter -> getUsersOfferingScore($item ['uid'])[1] . ')</p>
										</div>

										<p class="offeringpostlabel">Offering:</p>
										<p class="userinputtedfield">' . $item ['body_description'] . '</p>

										<div class="postcontrols">
											<img class="postoptionsbutton" src="assets/images/graphics/optionbutton.png">
											<img class="pinpostbutton" src="assets/images/graphics/pinpostbutton.png">
											<img class="expandpostbutton" src="assets/images/graphics/arrowexpand.png">
										</div>
									</div>';
						
						echo $result;
					}
				}
			?>
		</div>

		<div id="SeekingSection" class="boardcontent">
			<?php
				if (isset ( $_GET ['category'] ) && ($_GET ['category'] !== 'All Categories')) {
					$arr = $accountDatabaseAdapter->searchSeekingPosts ( $_GET ['category'] );
				}
				else {
					$arr = $accountDatabaseAdapter->getAllSeekingPosts ();
				}
				
				if (sizeof ( $arr ) == 0 && isset($_GET ['category'])) {
					$result = 'There are currently no seeking posts for ' . $_GET ['category'];
					echo $result;
				} else {
					$result = '';
					
					foreach ( array_reverse ( $arr ) as $item ) {
						$result = '<div class="seekingposttemplate">
										<div class="nameandoccupation">
											<p class="usersnamefield">' . $accountDatabaseAdapter -> getUsersName($item ['uid']) . '</p>
											<p class="userinputtedoccupation">' . $item ['occupation'] . '</p>
										</div>
										<div class="seekinguserratingbox">
											<p class="seekingrating">' . $accountDatabaseAdapter -> getUsersSeekingScore($item ['uid'])[0] . '</p>
											<p class="seekingnumofrating">(' . $accountDatabaseAdapter -> getUsersSeekingScore($item ['uid'])[1] . ')</p>
										</div>
										<p class="seekingpostlabel">Seeking:</p>
										<p class="userinputtedfield">' . $item ['body_description'] . '</p>
										<div class="postcontrols">
											<img class="postoptionsbutton" src="assets/images/graphics/optionbutton.png">
											<img class="pinpostbutton" src="assets/images/graphics/pinpostbutton.png">
											<img class="expandpostbutton" src="assets/images/graphics/arrowexpand.png">
										</div>
									</div>';
						echo $result;
					}
				}
			?>
		</div>

		<div id="MyProfileSection" class="boardcontent">
			<?php
				if (isset ( $_GET ['category'] ) && ($_GET ['category'] !== 'All Categories')) {
					$arr = $accountDatabaseAdapter->searchPersonalPosts ( $_SESSION ['user_email'], $_GET ['category'] );
				} 
				else {
					$arr = $accountDatabaseAdapter->getAllPersonalPosts ( $_SESSION ['user_email'] );
				}
				
				if (sizeof ( $arr ) == 0 && isset($_GET ['category'])) {
					$result = 'You have no seeking or offering posts for ' . $_GET ['category'];
					echo $result;
				} 
				else {
					$result = '';
					
					if (sizeof ( $arr ) == 0) {
						echo "You currently have no active posts.";
					}
					
					foreach ( array_reverse ( $arr ) as $item ) {
						if ($item ['type'] === 'seeking') {
							$result = '<div class="seekingposttemplate">
											<div class="nameandoccupation">
											<p class="usersnamefield">' . $_SESSION ['user_first_name'] . '</p>
											<p class="userinputtedoccupation">' . $item ['occupation'] . '</p>
										</div>
										<div class="seekinguserratingbox">
											<p class="seekingrating">' . $accountDatabaseAdapter -> getUsersSeekingScore($item ['uid'])[0] . '</p>
											<p class="seekingnumofrating">(' . $accountDatabaseAdapter -> getUsersSeekingScore($item ['uid'])[1] . ')</p>
										</div>
										<p class="seekingpostlabel">Seeking:</p> <p class="userinputtedfield">' . $item ['body_description'] . '</p>
										<div class="postcontrols">
											<img class="postoptionsbutton" src="assets/images/graphics/optionbutton.png">
											<img class="pinpostbutton" src="assets/images/graphics/pinpostbutton.png">
											<img class="expandpostbutton" src="assets/images/graphics/arrowexpand.png">
										</div>
									</div>';
						}
						
						else {
							$result = '<div class="offeringposttemplate">
											<div class="nameandoccupation">
												<p class="usersnamefield">' . $_SESSION ['user_first_name'] . '</p>
												<p class="userinputtedoccupation">' . $item ['occupation'] . '</p>
											</div>
											<div class="offeringuserratingbox">
												<p class="offeringrating">' . $accountDatabaseAdapter -> getUsersOfferingScore($item ['uid'])[0] . '</p>
												<p class="offeringnumofrating">(' . $accountDatabaseAdapter -> getUsersOfferingScore($item ['uid'])[1] . ')</p>
											</div>
											<p class="offeringpostlabel">Offering:</p>
											<p class="userinputtedfield">' . $item ['body_description'] . '</p>
											<div class="postcontrols">
												<img class="postoptionsbutton" src="assets/images/graphics/optionbutton.png">
												<img class="pinpostbutton" src="assets/images/graphics/pinpostbutton.png">
												<img class="expandpostbutton" src="assets/images/graphics/arrowexpand.png">
											</div>
										</div>';
						}
						echo $result;
					}
				}
			?>
		</div>


		<?php
			if ($accountDatabaseAdapter -> getUserType($_SESSION ['user_id']) == "admin") {
				echo "<div id='AdminPanelSection' class='boardcontent'>
						<p>Reset A User's Password</p>
						<input id='user_email_pass_reset' type='text' name='user_email' placeholder='User's Email' autofocus required>
						<input id='pass_pass_reset' type='text' name='reset_password' placeholder='New Password' required>
						<button class='admin_rpassword'>Submit</button>
						<p id='reset_pass_response'></p>
					</div>";}
		?>
	</div>


	<div id="createpostbox" class="cpbox">
		<div class="cp-content">
			<span class="close">&times;</span>


			<div id="createposttypepicker">
				<button class="pickerbutton"
					onclick="offeringSeekingSelection('createoffering')">Offering</button>
				or
				<button class="pickerbutton"
					onclick="offeringSeekingSelection('createseeking')">Seeking</button>
			</div>

			<form id="formOffering" action="assets/scripts/controller.php"
				method="post">
				<input type="hidden" name="form" value="formOffering" />
				<div id="createoffering" class="createposttemplate">
					<p class="postcontentname">My name is <?php echo $_SESSION['user_first_name']; ?>. I am a...</p>
					<input id="createpostoccupationoffering" type="text" name="createpostoccupationoffering" maxlength="20" placeholder="Your Occupation" required>
					<p class="postcontenttext">looking for people that need...</p>
					<textarea id="createpostdescriptionoffering" name="createpostdescriptionoffering" placeholder="Description"	required></textarea>


					<div id="createpostoptions">
							Category <select id="createpostcategories"
								name="createpostcategories">
								<?php echo file_get_contents('./assets/categories.txt', true, NULL, 1);?>
							</select>
						<button id="createpostbutton">Create Post</button>
					</div>
				</div>
			</form>

			<form id="formSeeking" action="assets/scripts/controller.php"
				method="post">

				<input type="hidden" name="form" value="formSeeking" />

				<div id="createseeking" class="createposttemplate">
					<p class="postcontentname">My name is <?php echo $_SESSION['user_first_name']; ?>. I'm looking for a...</p>
					<input id="createpostoccupationseeking" type="text" name="createpostoccupationseeking" placeholder="Occupation" required>
					<p class="postcontenttext">for help with...</p>
					<textarea id="createpostdescriptionseeking"	name="createpostdescriptionseeking" placeholder="Description" required></textarea>
					<p class="postcontenttext"> by... <input id="createpostduedate" type="date"	name="createpostduedate" required></p>
				
					<div id="createpostoptions">
							Category <select id="createpostcategories"	name="createpostcategories2">
								<?php echo file_get_contents('./assets/categories.txt', true, NULL, 1);?>
							</select>
						<button id="createpostbutton2">Create Post</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</body>

<script>
	function fittext(){
		var maxW = 105, maxH = 20, maxSize = 20;
		var c = document.getElementsByClassName("userinputtedoccupation");
		var d = document.createElement("span");
		d.style.fontSize = maxSize + "px";

		for (var i = 0; i < c.length; i++){
			d.innerHTML = c[i].innerHTML;
			document.body.appendChild(d);
			var w = d.offsetWidth;
			var h = d.offsetHeight;
			document.body.removeChild(d);
			var x = w > maxW ? maxW / w : 1;
			var y = h > maxH ? maxH / h : 1;
			var r = Math.min(x, y) * maxSize;
			c[i].style.fontSize = r + "px";
    	}
	}
	fittext();
</script>
<?php
	if ($accountDatabaseAdapter -> getUserType($_SESSION ['user_id']) == "admin") {
		echo "<script type='text/javascript' src='assets/js/admincontrols.js'></script>";
	}
?>
<script type="text/javascript" src="assets/js/tabs.js"></script>
<script type="text/javascript" src="assets/js/createpostbox.js"></script>
</html>