var currentUrl = window.location.href;

if (currentUrl.indexOf("#") == -1)
    document.getElementById("OfferingButton").click();
else {
    var currPage = currentUrl.split("#");
    openPage(currPage[1]);
}

function openPage(tabName) {
    var i, boardcontent, sectionl;
    boardcontent = document.getElementsByClassName("boardcontent");
    for (i = 0; i < boardcontent.length; i++) {
        boardcontent[i].style.display = "none";
    }
    sectionl = document.getElementsByClassName("sectionl");
    for (i = 0; i < sectionl.length; i++) {
        sectionl[i].className = sectionl[i].className.replace(" active", "");
    }
    document.getElementById(tabName + "Section").style.display = "block";
    document.getElementById(tabName + "Button").className += " active";
    window.location.href = "#" + tabName;

    // Sends current page to Google Analytics every time a new tab is clicked
    ga('send', 'pageview', {'page': location.pathname + location.search  + location.hash});
}

function offeringSeekingSelection(pick) {
    var i, postType, pickerbutton;
    postType = document.getElementsByClassName("createposttemplate");
    for (i = 0; i < postType.length; i++) {
        postType[i].style.display = "none";
    }
    pickerbutton = document.getElementsByClassName("pickerbutton");
    for (i = 0; i < pickerbutton.length; i++) {
        pickerbutton[i].className = pickerbutton[i].className.replace(" active", "");
    }
    document.getElementById(pick).style.display = "block";
    document.getElementById("createpostoptions").style.display = "block";
    document.getElementById("createpostoptions2").style.display = "block";
    event.currentTarget.className += " active";
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}