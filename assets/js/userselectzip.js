$(function() {

    // User clicks the zip code text
    $("#location").click(function() {
        var userZip = $(".userzip").text();
        if(userZip.length > 5)
            $(".userzip").html("<input class='userinputzip' type='text' value='55555'>");
        else
            $(".userzip").html("<input class='userinputzip' type='text' value='" + userZip + "'>");
        $(".userzip").attr("class", "userzipinputbox");

        $(".userinputzip").attr({
            "maxlength" : 5,      
            "minlength" : 5
        });
        $(".userinputzip").select();

        // Waits for user to click enter
        $(".userinputzip").on("keypress" ,function(key){
            if (key.which == 13){
                var newZip = $(".userinputzip").val();
                var isZip = (/^\d{5}$/.test(newZip));

                if(isZip){
                    $(".userzipinputbox").attr("class", "userzip");
                    $(".userzip").html(newZip);

                    if(userZip != newZip){
                        // Need to update Offering and Seeking post with new user's zip
                        $.post("assets/scripts/controller.php", {newuserzip: newZip},
                        function(status){
                        
                        });
                    }
                }
            }
        });

        // Waits for user to click outside of the box (unfocus)
        $(".userinputzip").on("blur" ,function(key){
            var newZip = $(".userinputzip").val();
            var isZip = (/^\d{5}$/.test(newZip));

            if(isZip){
                $(".userzipinputbox").attr("class", "userzip");
                $(".userzip").html(newZip);

                if(userZip != newZip){
                    // Need to update Offering and Seeking post with new user's zip
                    $.post("assets/scripts/controller.php", {newuserzip: newZip},
                    function(status){
                        
                    });
                }
            }
        });
    });
});