// Gets zip code based on user's IP address from https://ipinfo.io/json
$.getJSON("https://ipinfo.io/json", function(data, status){
	var isZip = (/^\d{5}$/.test(data["postal"]));
	if(status == "success" && isZip)
		$("#location").html("Location: " + data["postal"]);
	else
		$("#location").html("Location: cannot be determined");
});