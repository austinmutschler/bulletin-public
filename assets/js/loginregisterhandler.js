$(function() {

    // User Clicks Login
    $("#login-button").click(function() {
        login();
    });

    // User Presses Enter at login
    $("#loginformblock").on("keypress" ,function(key){
        if(key.which == "" || key.which == 13){
            login();
        }
    });
    
    $(".input-login-email").on("keypress" ,function(key){
        $(".input-login-email").css({
                backgroundColor:"rgba(0, 0, 0, 0.4)"
        });
    });
    
    function login(){
        userEmail = $(".input-login-email").val();
        userPassword = $(".input-login-password").val();

        // Add more error checking
        if(!userEmail.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            $(".input-login-email").select();
            setErrorMessage("login", "Invalid Email Address")
            $(".input-login-email").css({
                backgroundColor:"rgba(255, 51, 51, 0.5)"
            });
        }
        else{
            // After Validity Regex Check, Post input data to controller
            hideErrorMessage();

            $.post("assets/scripts/controller.php", {
                loginemail: userEmail,
                loginpassword: userPassword
            },function(data, status){
                if(status === "success"){
                    if (data === "login")
                        window.location = "./board.php";
                    else
                        setErrorMessage("login", data);
                }
                else{
                    setErrorMessage("login", "Internal Server Error");
                }
            });
        }
    }

    function setErrorMessage(page, message){
        $("#error-message").css({
            visibility:"visible",
            overflow:"hidden",
            maxWidth:"280px"  
        });
        $("#error-message").html(message);
    }

    function hideErrorMessage(){
        $("#error-message").css({
            visibility:"hidden",
            maxWidth:"0px"  
        });
        $("#error-message").html("");
    }
});