<?php
session_start();
date_default_timezone_set('America/Phoenix');
require_once 'model.php';
$accountDatabaseAdapter = new accountDatabaseAdapter();


// Account Registration
if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password']) ){
	
	function register_captcha($user_response) {
        $fields_string = '';
        $fields = array(
            'secret' => '6LfnVyIUAAAAAEQxTUDw86YUQQm48-8a2-6nMag3',
            'response' => $user_response
        );
        foreach($fields as $key=>$value)
        $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    // Call the function post_captcha
    $res = register_captcha($_POST['g-recaptcha-response']);

    if (!$res['success']) {
        // What happens when the CAPTCHA is bad
        header("Location: ../../register.php?error=badCaptcha");
    } else {
        // If CAPTCHA is successfully completed...

		$firstname =  htmlspecialchars($_POST ['firstname']);
		$lastname = htmlspecialchars($_POST ['lastname']);
		$email = htmlspecialchars($_POST ['email']) ;
		$password =  htmlspecialchars($_POST ['password']);
		$ip = $_SERVER['REMOTE_ADDR'];
		$userIpInfo = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));

		// Add error checking to see if server returns a zip code before issuing session
		$userZipCode = $userIpInfo->{'postal'};
		
		$existsInDB = $accountDatabaseAdapter -> userExistInDB($email);
		
		if($existsInDB){
			echo "Email Address already exists";
		}
		else{
			//create account
			$accountDatabaseAdapter -> createAccount($firstname, $lastname, $email, $password);
		
			//set session variable
			$user_id = $accountDatabaseAdapter -> getUserId($email);
			$first_name = $accountDatabaseAdapter ->getUsersName($user_id);
			$_SESSION['user_id'] = $user_id;
			$_SESSION['user_first_name'] = $first_name;
			$_SESSION['user_email'] = $email;
			$_SESSION["user_zip"] = $userZipCode;
		
			// Redirect to Board Page.
			header ( "Location: ../../board.php" );
			exit();
		}
	}
}

// Account Login
else if(isset($_POST['loginemail']) && isset($_POST['loginpassword'])){
	
	$email = htmlspecialchars($_POST ['loginemail']);
	$password =  htmlspecialchars($_POST ['loginpassword']);
	$ip = $_SERVER['REMOTE_ADDR'];
	$validIP = filter_var($ip, FILTER_VALIDATE_IP);
	if($validIP){
		$userIpInfo = json_decode(file_get_contents("http://ipinfo.io/json"));
		if(isset($userIpInfo->{'postal'})){
			if(strlen($userIpInfo->{'postal'}) == 5){
				$userZipCode = $userIpInfo->{'postal'};
			}
		}
		else
		$userZipCode = "";
	}
	else
		$userZipCode = "";

	$existsInDB = $accountDatabaseAdapter -> userExistInDB($email);
	
	// Wrong email
	if(!$existsInDB){
		echo "Email Address or Password Incorrect";
	}

	else{
		// Checks password for accuracy
		$userVerfied = $accountDatabaseAdapter ->verifyLogin($email, $password);
		
		// If password is wrong
		if(!$userVerfied){
			echo "Email Address or Password Incorrect";
		}
		else{
			// Set session variables
			$user_id = $accountDatabaseAdapter ->getUserId($email);
			$first_name = $accountDatabaseAdapter ->getUsersName($user_id);
			$_SESSION['user_id'] = $user_id ;
			$_SESSION['user_first_name'] = $first_name;
			$_SESSION['user_email'] = $email;
			$_SESSION["user_zip"] = $userZipCode;
			
			// Triggers loginregisterhandler javascript to redirect the user to board page
			echo "login";
			exit();
		}
		
	}

}

//OFFERING POST
else if($_POST['form'] === 'formOffering'){

	$accountDatabaseAdapter ->createOfferingPost();
	
	header ( "Location: ../../board.php" );
}

//SEEKING POST
else if($_POST['form'] === 'formSeeking'){

	$accountDatabaseAdapter ->createSeekingPost();
	

	header ( "Location: ../../board.php#Seeking" );

}

//Searching a category
else if ($_POST['form'] === 'searchSeekingOffering'){
	
	$location = "Location: ../../board.php?category=" . $_POST['categories'];
	header($location);
}

else if(isset($_POST['newuserzip'])){
	$accountDatabaseAdapter -> updateUserZip($_POST['newuserzip']);
	echo "Updated";
	exit();
}

?>