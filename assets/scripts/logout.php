<?php
session_start();
date_default_timezone_set('America/Phoenix');

//End Session
session_destroy();

//Redirect to Login Page
header ( "Location: ../../login.php" );
exit();
?>