<?php
date_default_timezone_set('America/Phoenix');

class adminPanelController {

	private $DB;
	
	// establish connection to the database named 'bulletin_db'
	public function __construct() {
		$db = 'mysql:dbname=bulletin_db;host=127.0.0.1;charset=utf8';
		$user = 'model';
		$password = 'PRJiqO7CZiPZXDf4';
		
		try {
			$this->DB = new PDO ( $db, $user, $password );
			$this->DB->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $e ) {
			echo ('Error Establishing Connection');
			exit ();
		}
	}

	public function isAdmin($uid) {
		$stmt = $this->DB->prepare ( "SELECT acct_type FROM user_account WHERE ID= :uid" );
		$stmt->bindParam ('uid', $uid );
		$stmt->execute ();
		$acct_type = $stmt->fetchColumn();
		if($acct_type == 'admin')
			return TRUE;
		else
			return FALSE;
	}

	public function changeUserPassword($email, $newPassword) {
		require_once 'model.php';
		$accountDatabaseAdapter = new accountDatabaseAdapter();

		$email = trim($email);
		$uid = $accountDatabaseAdapter -> getUserId($email);
		$hashed_pwd = password_hash ( $newPassword, PASSWORD_DEFAULT );

		$stmt = $this->DB->prepare ( "UPDATE user_account SET password = :password WHERE user_account.ID = :uid;" );
		$stmt->bindParam('password', $hashed_pwd);
		$stmt->bindParam('uid', $uid);
		$stmt->execute ();
		echo ('Success');
	}	
}
?>