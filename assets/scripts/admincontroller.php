<?php
session_start();
date_default_timezone_set('America/Phoenix');
require_once 'model.php';
require_once 'adminpanelcontrols.php';
$accountDatabaseAdapter = new accountDatabaseAdapter();
$adminPanelController = new adminPanelController();

// Checks that user is actually and administrator
if(!isset($_SESSION['user_id']))
	exit('Access Denied: You are not logged in.');

if($adminPanelController -> isAdmin($_SESSION['user_id']) == FALSE)
	exit('Access Denied: You are not logged in to an administrator account. Your Account Information & IP Address have been logged.');
	// TODO: Actually log user IP address and account info into a database table 


// User Password Reset
if(isset($_POST['user_email']) && isset($_POST['reset_password'])){
	
		$user_email = $_POST['user_email'];
		$reset_password = $_POST['reset_password'];
		
		$existsInDB = $accountDatabaseAdapter -> userExistInDB($user_email);
		
	if($existsInDB){
		$adminPanelController -> changeUserPassword($user_email, $reset_password);
	}
	else{
		exit('Error: That email does not exist.');
	}

}

//LOGIN TO ACCOUNT
else if(isset($_POST['email']) && isset($_POST['password'])){
	
	$email = htmlspecialchars($_POST ['email']);
	$password =  htmlspecialchars($_POST ['password']);
	$existsInDB = $accountDatabaseAdapter -> userExistInDB($email);
	
	//wrong username
	if(!$existsInDB){
		header("Location: ../../login.php?error=badCreds");
		
	}
	else{
		//check password
		$userVerfied = $accountDatabaseAdapter ->verifyLogin($email, $password);
		
		//wrong password
		if(!$userVerfied){
			header("Location: ../../login.php?error=badCreds");
		}
		else{
			//set session variable
			$user_id = $accountDatabaseAdapter ->getUserId($email);
			$first_name = $accountDatabaseAdapter ->getUsersName($user_id);
			$_SESSION['user_id'] = $user_id ;
			$_SESSION['user_first_name'] = $first_name;
			$_SESSION['user_email'] = $email;
			
			// Redirect to Board Page.
			header ( "Location: ../../board.php" );
			exit();
		}
		
	}

}

?>