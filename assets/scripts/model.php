<?php

class accountDatabaseAdapter {
	private $DB;
	
	// Connecting to main database: bulletin_db
	public function __construct() {
		date_default_timezone_set('America/Phoenix');

		$db = 'mysql:dbname=bulletin_db;host=127.0.0.1;charset=utf8';
		$user = 'model';
		$password = 'PRJiqO7CZiPZXDf4';
		
		try {
			$this->DB = new PDO ( $db, $user, $password );
			$this->DB->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $e ) {
			echo ('Error Establishing Connection');
			exit ();
		}
	}
	public function createAccount($firstname, $lastname, $email, $pwd) {
		
		// Trimming input
		$firstname = trim ( $firstname );
		$lastname = trim ( $lastname );
		$email = trim ( $email );
		$date_time = date("m/d/Y" . " | " . "h:i:sA");
		
		// Security: Hashing and Salting Password
		$hashed_pwd = password_hash ( $pwd, PASSWORD_DEFAULT );
		
		// Adds all user information into database
		$stmt1 = $this->DB->prepare ( "INSERT INTO user_account (date_created, first_name, last_name, email, password) VALUES (:datetime, :firstname, :lastname, :email, :hashed_pwd);");
		
		$stmt1->bindParam('datetime', $date_time);
		$stmt1->bindParam('firstname', $firstname);
		$stmt1->bindParam('lastname', $lastname);
		$stmt1->bindParam('email', $email);
		$stmt1->bindParam('hashed_pwd', $hashed_pwd);
		
		$stmt1->execute ();

		// Get User ID from db
		require_once 'model.php';
		$accountDatabaseAdapter = new accountDatabaseAdapter();
		$uniqueID = $accountDatabaseAdapter -> getUserId($email);

		// Creates score record for new user
		$stmt2 = $this->DB->prepare ( "INSERT INTO user_rating_score (uid) VALUES (:userID);");
		$stmt2->bindParam('userID', $uniqueID);
		$stmt2->execute ();
		
		return true;
	}
	
	
	public function userExistInDB($email) {
		$stmt = $this->DB->prepare ( "SELECT email FROM user_account WHERE email= :email" );
		$stmt->bindParam ( 'email', $email );
		$stmt->execute ();
		
		$result = $stmt->rowCount();
		if ($result == 0) 
			return false; // No duplicate emails in the database
		else 
			return true; // Email already exists in database
	}
	
	public function verifyLogin($email, $pwd){
		$stmt = $this->DB->prepare ( "SELECT password FROM user_account WHERE email= :email" );
		$stmt->bindParam ('email', $email );
		$stmt->execute ();
		$db_pwd = $stmt->fetchColumn();
		
		$isValid = password_verify($pwd, $db_pwd);
		
		if($isValid)
			return true; // Password Valid
		else
			return false; // Password invalid
	}
	
	public function getUsersName($uid){
		$stmt = $this->DB->prepare ( "SELECT first_name FROM user_account WHERE ID= :uid" );
		$stmt->bindParam ('uid', $uid );
		$stmt->execute ();
		$name = $stmt->fetchColumn();
		
		return $name;
	}

	public function getUserId($email){
		$stmt = $this->DB->prepare ( "SELECT ID FROM user_account WHERE email= :email" );
		$stmt->bindParam ('email', $email);
		$stmt->execute ();
		$user_id = $stmt->fetchColumn();
		
		return $user_id;
	}

	public function getUserType($uid){
		$stmt = $this->DB->prepare ( "SELECT acct_type FROM user_account WHERE ID= :uid" );
		$stmt->bindParam ('uid', $uid);
		$stmt->execute ();
		$account_type = $stmt->fetchColumn();
		
		return $account_type;
	}

	public function updateUserZip($updateUserZip){
		$_SESSION['user_zip'] = $updateUserZip;
	}

	public function getUsersOfferingScore($uid){
		$stmt = $this->DB->prepare ( "SELECT offering_score FROM user_rating_score WHERE uid= :uid" );
		$stmt->bindParam ('uid', $uid);
		$stmt->execute ();
		$score = $stmt->fetchColumn();
		$score = number_format($score, 2);

		$stmt = $this->DB->prepare ( "SELECT num_of_offering_reviews FROM user_rating_score WHERE uid= :uid" );
		$stmt->bindParam ('uid', $uid);
		$stmt->execute ();
		$numreviews = $stmt->fetchColumn();
		$numreviews = number_format($numreviews);

		$scoredata = array($score, $numreviews);

		return $scoredata;
	}

	public function getUsersSeekingScore($uid){
		$stmt = $this->DB->prepare ( "SELECT seeking_score FROM user_rating_score WHERE uid= :uid" );
		$stmt->bindParam ('uid', $uid);
		$stmt->execute ();
		$score = $stmt->fetchColumn();
		$score = number_format($score, 2);

		$stmt = $this->DB->prepare ( "SELECT num_of_seeking_reviews FROM user_rating_score WHERE uid= :uid" );
		$stmt->bindParam ('uid', $uid);
		$stmt->execute ();
		$numreviews = $stmt->fetchColumn();
		$numreviews = number_format($numreviews);

		$scoredata = array($score, $numreviews);

		return $scoredata;
	}
	
	public function createOfferingPost(){
				
		//set remaining variables
		$type = 'offering';
		$user_id = $_SESSION['user_id'];
		$date_time = date("m/d/Y" . " | " . "h:i:sA");
		$location = $_SESSION ['user_zip'];
		$category = $_POST['createpostcategories'];
		$occupation = $_POST['createpostoccupationoffering'];
		$description = $_POST['createpostdescriptionoffering'];
		$contact = $_SESSION['user_email'];

		
		//INSERT into DB
		$stmt = $this->DB->prepare ("INSERT INTO post (type, uid, creation_date_time, location, category, occupation, body_description, contact) VALUES (:type, :uid, :datetime, :location, :category, :occupation, :description, :contact);");
		$stmt->bindParam('type', $type);
		$stmt->bindParam('uid', $user_id);
		$stmt->bindParam('datetime', $date_time);
		$stmt->bindParam('location', $location);
		$stmt->bindParam('category', $category);
		$stmt->bindParam('occupation', $occupation);
		$stmt->bindParam('description', $description);
		$stmt->bindParam('contact', $contact);
		$stmt->execute ();
		
		return true;
	}
	
	public function createSeekingPost(){
		
		// set remaining variables
		$type = 'seeking';
		$user_id = $_SESSION['user_id'];
		$date_time = date("m/d/Y" . " | " . "h:i:sA");
		$location = trim($_SESSION ['user_zip']);
		$category = $_POST['createpostcategories2'];
		$occupation = $_POST['createpostoccupationseeking'];
		$description = $_POST['createpostdescriptionseeking'];
		$due_date_arr = explode('-', $_POST['createpostduedate']);
		$due_date = $due_date_arr[1] . '/' . $due_date_arr[2] . '/' . $due_date_arr[0]; // MM/DD/YYYY format
		$contact = $_SESSION['user_email'];
		
		// INSERT into DB
		$stmt = $this->DB->prepare ("INSERT INTO post (type, uid, creation_date_time, location, category, occupation, body_description, due_date, contact) VALUES (:type, :uid, :datetime, :location, :category, :occupation, :description, :dueDate, :contact);");
		$stmt->bindParam('type', $type);
		$stmt->bindParam('uid', $user_id);
		$stmt->bindParam('datetime', $date_time);
		$stmt->bindParam('location', $location);
		$stmt->bindParam('category', $category);
		$stmt->bindParam('occupation', $occupation);
		$stmt->bindParam('description', $description);
		$stmt->bindParam('dueDate', $due_date);
		$stmt->bindParam('contact', $contact);
		$stmt->execute ();
	}
	
	
	public function getAllOfferingPosts(){
		$stmt = $this->DB->prepare ( "SELECT * FROM post WHERE post.hidden != 1 AND type='offering';" );
		$stmt->execute ();
		
		$arr = $stmt->fetchAll();
		
		return $arr;
	}
	
	public function getAllSeekingPosts(){
		$stmt = $this->DB->prepare ( "SELECT * FROM post WHERE post.hidden != 1 AND type='seeking';" );
		$stmt->execute ();
		
		$arr = $stmt->fetchAll();
		
		return $arr;
	}
	
	
	public function getAllPersonalPosts($currUser){
		$query = 'SELECT user_account.email, post.type, post.uid, post.category, post.occupation, post.body_description, post.due_date, post.contact FROM post JOIN user_account ON user_account.email = post.contact WHERE post.hidden != 1 AND user_account.email = "' . $currUser . '";';
		
		$stmt = $this->DB->prepare($query);
		$stmt->execute ();
		
		$arr = $stmt->fetchAll();
		
		return $arr;
	}
	
	public function searchOfferingPosts($category){
		$query = "SELECT * FROM post WHERE post.hidden != 1 AND type ='offering' AND category='" . $category . "';";
		$stmt = $this->DB->prepare ( $query ); 
		$stmt->execute ();
		
		$arr = $stmt->fetchAll();
		
		return $arr;
	}
	
	public function searchSeekingPosts($category){
		$query = "SELECT * FROM post WHERE post.hidden != 1 AND type ='seeking' AND category='" . $category . "';";
		$stmt = $this->DB->prepare ( $query );
		$stmt->execute ();
		
		$arr = $stmt->fetchAll();
		
		return $arr;
	}
	
	public function searchPersonalPosts($currUser, $category){
		$query = 'SELECT user_account.email, post.type, post.uid, post.category, post.occupation, post.body_description, post.due_date, post.contact FROM post JOIN user_account ON user_account.email = post.contact WHERE post.hidden != 1 AND user_account.email = "' . $currUser . '" AND post.category="' . $category . '";';
		
		$stmt = $this->DB->prepare($query);
		$stmt->execute ();
		$arr = $stmt->fetchAll();
		
		return $arr;
	}
} // end class acccountDatabaseAdapter
?>