<?php
session_start ();

// If user is logged in
if (isset ( $_SESSION ['user_id'] )) {
	header ( "Location: board.php" );
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Bulletin - Register</title>
<link href="assets/style/start.css" type="text/css" rel="stylesheet">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Roboto|Titillium+Web" rel="stylesheet">
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-100420011-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
	<div id="overlay">
		<div id="formcontainer">
			<p id="registerheader">Bulletin</p>
			
			<form action="assets/scripts/controller.php" method="post">
			<div id="registerformblock">
				<input type="text" name="firstname" placeholder="First Name" autofocus required>
				<input type="text" name="lastname"	placeholder="Last Name" required>
				<input type="email" name="email" placeholder="Email" required>
				<input type="password" name="password" placeholder="Password" required>
				<div class="g-recaptcha" data-sitekey="6LfnVyIUAAAAAPNcdw0Hwyg2MrQSu6GdSKDE2iY1" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;width: 77%;"></div>
				<p class="register_login">
					<input class="mainbuttons" type="submit" name="register" value="Register"> <a class="elsetext"> or </a> <a class="orhyper"	href="login.php">Login</a>
				</p>
			<div>
			</form>
			<?php
				if (isset ( $_GET ['error'] )&& ($_GET['error'] == 'duplicateCreds')) {
					echo '<div id="errormessage"> Account Already Exists </div>';
				}
				else if (isset ( $_GET ['error'] )&& ($_GET['error'] == 'badCaptcha')) {
					echo '<div id="errormessage"> Incorrect Captcha </div>';
				}
			?>
		</div>
	</div>
</body>
</html>