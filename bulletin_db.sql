-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2018 at 11:47 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulletin_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `ID` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `uid` int(11) NOT NULL,
  `creation_date_time` varchar(50) NOT NULL,
  `location` int(5) NOT NULL,
  `category` varchar(20) NOT NULL,
  `occupation` varchar(40) NOT NULL,
  `body_description` varchar(255) NOT NULL,
  `due_date` varchar(20) DEFAULT NULL,
  `contact` varchar(60) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`ID`, `type`, `uid`, `creation_date_time`, `location`, `category`, `occupation`, `body_description`, `due_date`, `contact`, `hidden`) VALUES
(1, 'offering', 1, '05-20-2017', 85719, 'Software Development', 'Web Developer', 'help building websites', '', 'austinmutschler@gmail.com', 0),
(2, 'seeking', 2, '05-20-2017', 85719, 'Software Development', 'Web Developer', 'websites', '05/26/2017', 'austinmutschler@gmail.com', 0),
(3, 'offering', 1, '05-20-2017', 85719, 'Legal', 'Test', 'This is a test', '', 'austinmutschler@gmail.com', 0),
(4, 'offering', 3, '05-21-2017', 85719, 'Automotive', 'Mechanic', 'people', '', 'test@test.com', 0),
(5, 'seeking', 1, '05-21-2017', 85719, 'Labor/Move', 'person', 'moving to a new apartment', '05/23/2017', 'austinmutschler@gmail.com', 0),
(6, 'offering', 1, '05-21-2017', 85719, 'Automotive', 'Hidden Post', 'Do you see me?', '', 'austinmutschler@gmail.com', 1),
(8, 'offering', 6, '05-29-2017', 85719, 'Automotive', 'HIT Mentor/Guru', 'Greetings from across the plains of existence spreading the message of peace and love through reliable and reasonably priced computer repair.', '', 'averycdavidson@gmail.com', 0),
(9, 'offering', 6, '05-29-2017', 85719, 'Pet', 'Dog Walker', 'help walking any animals. I am available weekends and weeknights.', '', 'averycdavidson@gmail.com', 0),
(10, 'offering', 6, '05-29-2017', 85719, 'Computer', 'Information Security', 'This is a test for an information security post', '', 'averycdavidson@gmail.com', 0),
(11, 'offering', 1, '05-31-2017', 85719, 'Career/Job', 'Plumber', 'Amazing service at a cheap rate. From clogs to piping we have the reviews you can trust.', '', 'austinmutschler@gmail.com', 0),
(12, 'offering', 2, '05-31-2017', 85719, 'Automotive', 'Car Salesman', 'A brand new car that has the best price in the business!', '', 'austinmutschler@gmail.com', 0),
(13, 'offering', 1, '05-31-2017', 85719, 'Computer', 'Contractor', 'Surveying services for mountains and fields. Professionally Licensed', '', 'austinmutschler@gmail.com', 0),
(14, 'offering', 1, '05-31-2017', 85719, 'Career/Job', 'Plumber', 'Amazing service at a cheap rate. From clogs to piping we have the reviews you can trust.', '', 'austinmutschler@gmail.com', 0),
(15, 'offering', 6, '05-29-2017', 85719, 'Pet', 'Dog Runner', 'help walking any animals. I am available weekends and weeknights.', '', 'averycdavidson@gmail.com', 0),
(16, 'offering', 1, '05-31-2017', 85719, 'Computer', 'Contractor', 'Surveying services for mountains and fields. Professionally Licensed', '', 'austinmutschler@gmail.com', 0),
(17, 'offering', 6, '05-29-2017', 85719, 'Pet', 'A person that lovess', 'help walking any animals. I am available weekends and weeknights.', '', 'averycdavidson@gmail.com', 0),
(18, 'offering', 6, '05-29-2017', 85719, 'Automotive', 'HIT Mentor/Guru', 'Greetings from across the plains of existence spreading the message of peace and love through reliable and reasonably priced computer repair.', '', 'averycdavidson@gmail.com', 0),
(19, 'offering', 6, '05-29-2017', 85719, 'Pet', 'Walk Dogs', 'help walking any animals. I am available weekends and weeknights.', '', 'averycdavidson@gmail.com', 0),
(20, 'offering', 6, '05-29-2017', 85719, 'Computer', 'Information Security', 'This is a test for an information security post', '', 'averycdavidson@gmail.com', 0),
(21, 'offering', 1, '05-31-2017', 85719, 'Career/Job', 'Plumber', 'Amazing service at a cheap rate. From clogs to piping we have the reviews you can trust.', '', 'austinmutschler@gmail.com', 0),
(22, 'offering', 1, '05-31-2017', 85719, 'Automotive', 'Car Salesman', 'A brand new car that has the best price in the business!', '', 'austinmutschler@gmail.com', 0),
(23, 'offering', 1, '05-31-2017', 85719, 'Computer', 'Contractor', 'Surveying services for mountains and fields. Professionally Licensed', '', 'austinmutschler@gmail.com', 0),
(24, 'offering', 1, '05-31-2017', 85719, 'Career/Job', 'Plumber', 'Amazing service at a cheap rate. From clogs to piping we have the reviews you can trust.', '', 'austinmutschler@gmail.com', 0),
(25, 'offering', 6, '05-29-2017', 85719, 'Pet', 'A person that lovess', 'help walking any animals. I am available weekends and weeknights.', '', 'averycdavidson@gmail.com', 0),
(26, 'offering', 1, '05-31-2017', 85719, 'Computer', 'Contractor', 'Surveying services for mountains and fields. Professionally Licensed', '', 'austinmutschler@gmail.com', 0),
(27, 'offering', 6, '05-29-2017', 85719, 'Pet', 'A person that lovess', 'help walking any animals. I am available weekends and weeknights.', '', 'averycdavidson@gmail.com', 1),
(28, 'offering', 8, '06-04-2017', 85719, 'Legal', 'Robber', 'If you need to rob anyone, just hit me up. I am always down to rob peeps.', NULL, 'ranger@mybulletin.us', 1),
(29, 'offering', 8, '06-04-2017', 85719, 'Automotive', 'Baron', 'I sell cars and car parts', NULL, 'ranger@mybulletin.us', 0),
(30, 'seeking', 1, '06-04-2017', 85719, 'Household', 'Pumber', 'Redesigning my piping system in my bathroom. Pay is high if you\'re the right person.', '06/10/2017', 'austinmutschler@gmail.com', 0),
(31, 'seeking', 1, '06/04/2017 | 09:31:37AM', 85719, 'Computer', 'Electrician', 'I need someone to rewire my home audio system. I have a 6 speaker surround sound setup.', '06/07/2017', 'austinmutschler@gmail.com', 0),
(33, 'seeking', 9, '06/10/2017 | 01:04:21PM', 85001, 'Career/Job', 'Lower Contractor', 'doing a kitchen renovation in July', '07/03/2017', 'bob.builder@test.com', 0),
(34, 'offering', 11, '06/11/2017 | 06:32:14PM', 30350, 'Computer', 'Engineer', 'Help with computers', NULL, 'keith_berger@yahoo.com', 0),
(35, 'offering', 12, '08/05/2017 | 11:59:45AM', 85716, 'Therapeutic', 'test', 'testing this thing to see if the date and timestamp work properly', NULL, '12345@12345.com', 1),
(36, 'offering', 1, '09/01/2017 | 06:32:04PM', 85029, 'Automotive', 'person', 'cleaning', NULL, 'austinmutschler@gmail.com', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
